package my.code.z026.misc;

import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;

import com.google.gwt.core.client.EntryPoint;

/**
 * 
 * See: http://wotiamfinking.blogspot.co.uk/2008/09/write-your-own-junit-runner.html
 */
public class EntryPointRunner extends Runner {
	
	private Description description = Description.createSuiteDescription("Run Entry Point");
	
	private Class<? extends EntryPoint> targetClass;
	
	public EntryPointRunner(Class<? extends EntryPoint> targetClass) {
		this.targetClass = targetClass;
	}

	@Override
	public Description getDescription() {
		return description;
	}

	@Override
	public void run(RunNotifier notifier) {
		notifier.fireTestStarted(description);
		try {
			my.code.z026.util.RunnableEntryPoint.run(targetClass);
			//notifier.fireTestFinished(description);
		} catch (Exception e) {
			notifier.fireTestFailure(new Failure(description, e));
		}
	}

}
