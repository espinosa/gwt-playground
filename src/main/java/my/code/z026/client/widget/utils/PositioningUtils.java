package my.code.z026.client.widget.utils;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.Panel;

public class PositioningUtils {
	
	public static String getPanelDimensions(Panel panel) {
		Style s = panel.getElement().getStyle();
		return "Current shape: " +
		"offsetWidth: " + panel.getOffsetWidth() + ", offsetHeight: " + panel.getOffsetHeight() + ", " +
		"absoluteLeft: " + panel.getElement().getAbsoluteLeft() + ", absoluteTop: " + panel.getElement().getAbsoluteTop() + ", " +
		"absoluteRight: " + panel.getElement().getAbsoluteRight() + ", absoluteBottom: " + panel.getElement().getAbsoluteBottom() + ", " +
		"style.width=" + s.getWidth() + ", style.height=" + s.getHeight() + ", style.top=" + s.getTop() + ", style.left=" + s.getLeft()+ ", style.right=" + s.getRight() + ", style.bottom=" + s.getBottom();
	}
	
//	/**
//	 * Center the given reshapable widget against its parent.
//	 * @param widget
//	 */
//	public static void center(Reshapable widget) {
//		Widget parent = widget.getParent();
//		//int parentPanelWidth = Window.getClientWidth();
//		//int parentPanelHeight = Window.getClientHeight();
//		int parentPanelWidth = parent.getElement().getClientWidth(); // parent.getOffsetWidth();
//		int parentPanelHeight = parent.getElement().getClientHeight(); // parent.getOffsetHeight();
//		int newLeft = (parentPanelWidth - widget.getWidth())   / 2;
//		int newTop  = (parentPanelHeight - widget.getHeight()) / 2;
//		widget.reshape(newLeft, newTop, widget.getWidth(), widget.getHeight());
//	}
}
