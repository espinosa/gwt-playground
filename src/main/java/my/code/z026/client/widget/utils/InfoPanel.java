package my.code.z026.client.widget.utils;

import java.util.Map;

import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HTML;

public class InfoPanel extends AbsolutePanel {

	protected InfoPanel(int left, int top, Map<String, Object> properties, String title, InfoPanelTemplate template) {
		super();
		Style s = getElement().getStyle();
		s.setPosition(Position.ABSOLUTE);
		s.setTop(top, Unit.PX);
		s.setLeft(left, Unit.PX);
		s.setWidth(template.width, Unit.PX);
		s.setHeight(-1, Unit.PX);
		s.setOverflow(Overflow.HIDDEN);
		setStyleName(template.style);

		StringBuilder sb = new StringBuilder();
		if (title != null) {
			sb.append("<h1>" + title + "</h1>");
		}
//		for (Map.Entry<String, Object> property : properties.entrySet()) {
//			sb.append(property.getKey());
//			sb.append(" : ");
//			sb.append(property.getValue());
//			sb.append("<br>");
//		}
		sb.append("<table style=\"width: 100%\">");
		for (Map.Entry<String, Object> property : properties.entrySet()) {
			sb.append("<tr><td>");
			sb.append(property.getKey());
			sb.append("</td><td>");
			sb.append(property.getValue());
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		add(new HTML(sb.toString()));
	}

	public static class InfoPanelTemplate {
		private final String style;
		private final int width;

		public InfoPanelTemplate(String style, int width) {
			super();
			this.style = style;
			this.width = width;
		}

		public InfoPanel newPanel(int left, int top, Map<String, Object> properties, String title) {
			return new InfoPanel(left, top, properties, title, this);
		}
	}
}
