package my.code.z026.client.widget.utils;

/*

Also see:
http://dev.vaadin.com/svn/versions/6.8/src/com/vaadin/terminal/gwt/client/ComputedStyle.java
http://comments.gmane.org/gmane.org.google.gwt/58469
http://code.google.com/p/gwt-examples/source/browse/trunk/GoneVertical-Core/src/org/gonevertical/core/client/style/ComputedStyle.java
http://code.google.com/p/gwt-mosaic/source/browse/branches/dojo/src/gwt/mosaic/core/client/ComputedStyle.java
https://developer.mozilla.org/en-US/docs/DOM/window.getComputedStyle
http://javascript.info/tutorial/styles-and-classes-getcomputedstyle#getcomputedstyle-and-currentstyle
http://code.google.com/p/gwt-fx/issues/detail?id=122
*/

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Widget;

public class ComputedStyle {

    protected final JavaScriptObject computedStyle;
    private final Element elem;

    /**
     * Gets this element's computed style object which can be used to gather
     * information about the current state of the rendered node.
     * <p>
     * Note that this method is expensive. Wherever possible, reuse the returned
     * object.
     * 
     * @param elem
     *            the element
     * @return the computed style
     */
    @Deprecated
    public ComputedStyle(Element elem) {
        computedStyle = getComputedStyle(elem);
        this.elem = elem;
    }
    
    /**
     * Main constructor
     * @param widget
     */
    public ComputedStyle(Widget widget) {
    	if (!widget.isAttached()) {
    		throw new IllegalStateException(
    				"ComputedStyle cannot be established for detached elements. " +
    				"Some browsers, like WebKit, does not support getComputdStyle() on detached elements.");
    	}
        Element elem = widget.getElement();
        computedStyle = getComputedStyle(elem);
        this.elem = elem;
    }

    private static native JavaScriptObject getComputedStyle(Element elem)
    /*-{
      if(elem.nodeType != 1) {
          return {};
      }
      
      if($wnd.document.defaultView && $wnd.document.defaultView.getComputedStyle) {
          return $wnd.document.defaultView.getComputedStyle(elem, null);
      }
      
      if(elem.currentStyle) {
          return elem.currentStyle;
      }
    }-*/;
    
    /**
     * 
     * @param name
     *            name of the CSS property in camelCase
     * @return the value of the property, normalized for across browsers (each
     *         browser returns pixel values whenever possible).
     */
    public final native String getProperty(Element elem, String propertyName)
    /*-{
        
        var dashedPropertyName = propertyName.replace(/([A-Z])/g, "-$1").toLowerCase();
        
		if (elem.currentStyle) {
			r = elem.currentStyle[dashedPropertyName];
		}
		else if (window.getComputedStyle) {
			var cs = document.defaultView.getComputedStyle(elem, null)
			r = cs.getPropertyValue(dashedPropertyName);
		}
		return r;
        
    }-*/;

    /**
     * 
     * @param name
     *            name of the CSS property in camelCase
     * @return the value of the property, normalized for across browsers (each
     *         browser returns pixel values whenever possible).
     */
    public final native String getProperty(String propertyName)
    /*-{
        var cs = this.@my.code.z026.client.widget.utils.ComputedStyle::computedStyle;
        var elem = this.@my.code.z026.client.widget.utils.ComputedStyle::elem;
        
        var dashedPropertyName = propertyName.replace(/([A-Z])/g, "-$1").toLowerCase();
        
        if (cs.getPropertyValue)
           return cs.getPropertyValue(dashedPropertyName);
        else // IE
           return cs[dashedPropertyName];
        
    }-*/;

    public final int getIntPixelProperty(String propertyName) {
        String s = getProperty(propertyName);
        if (s!=null && s.endsWith("px")) {
        	return Integer.parseInt(s.substring(0, s.length() - 2));
        } else {
        	return 0;
        }
    }
    
    public final int getIntPixelProperty(Element elem, String propertyName) {
        String s = getProperty(elem, propertyName);
        if (s!=null && s.endsWith("px")) {
        	return Integer.parseInt(s.substring(0, s.length() - 2));
        } else {
        	return 0;
        }
    }
}

