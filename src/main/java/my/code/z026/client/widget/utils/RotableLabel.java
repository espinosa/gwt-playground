package my.code.z026.client.widget.utils;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.HTML;

/**
 * Define label with possibility to be rendered vertically.
 * http://stackoverflow.com/questions/1080792/how-to-draw-vertical-text-with-css-cross-browser
 * https://developer.mozilla.org/en-US/docs/Web/CSS/transform
 * https://developer.mozilla.org/en-US/docs/Web/CSS/transform-origin
 * Add support for full rotation later.
 * @since 25.6.2013
 */
public class RotableLabel extends HTML {

	private final boolean isVertical;

	public RotableLabel(String labelText, boolean isVertical) {
		super(labelText);
		this.isVertical = isVertical;
		if (isVertical) {
			Style s = getElement().getStyle();
			s.setProperty("WebkitTransform", "rotate(-90deg)");
			s.setProperty("MozTransform", "rotate(-90deg)");
			s.setProperty("OTransform", "rotate(-90deg)");
			s.setProperty("transform", "rotate(-90deg)");
			
			s.setProperty("WebkitTransformOrigin", "left top");
			s.setProperty("MozTransformOrigin", "left top");
			s.setProperty("OTransformOrigin", "left top");
			s.setProperty("transformOrigin", "left top");
		}
	}

	public boolean isVertical() {
		return isVertical;
	}
}
