package my.code.z026.client.widget.utils;

import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.Panel;

/**
 * Get principal position and shape properties from a UI panel widget. Try different variant
 * (client, offset) and sources (widget, element, style).
 * 
 * @author espinosa
 * @since 26.6.2013
 */
public class PositionIntrospector {
	
	/**
	 * Retrieve position and shape properties for the given panel.
	 * @param panel
	 * @return retrieved properties as map
	 */
	public Map<String, Object> getProperties(Panel panel) {
		Map<String, Object> p = new LinkedHashMap<String, Object>();

		p.put("offsetWidth", panel.getOffsetWidth());
		p.put("offsetHeight", panel.getOffsetHeight());
		p.put("absoluteLeft", panel.getElement().getAbsoluteLeft());
		p.put("absoluteTop", panel.getElement().getAbsoluteTop());
		p.put("absoluteRight", panel.getElement().getAbsoluteRight());
		p.put("absoluteBottom", panel.getElement().getAbsoluteBottom());

		Style s = panel.getElement().getStyle();
		p.put("style.width", s.getWidth());
		p.put("style.height", s.getHeight());
		p.put("style.top", s.getTop());
		p.put("style.left", s.getLeft());
		p.put("style.right", s.getRight());
		p.put("style.bottom", s.getBottom());

		return p;
	}
}
