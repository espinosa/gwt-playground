package my.code.z026.client.widget.utils;

import com.google.gwt.user.client.ui.Widget;

public class ComputedStyleInrospector {
	public String getSomeComputedStyleProperties(Widget w, String headerText) {
		ComputedStyle computedStyle = new ComputedStyle(w);
		StringBuilder sb = new StringBuilder();
		
		sb.append("<h1>" + headerText + "</h1>");
		
		sb.append("styleName: "           + w.getStyleName() + "<br>");
		sb.append("stylePrimaryName: "    + w.getStylePrimaryName() + "<br>");
		
		sb.append("backgroundColor: "    + computedStyle.getProperty("backgroundColor") + "<br>");
		sb.append("borderTopColor: "     + computedStyle.getProperty("borderTopColor")  + "<br>");
		sb.append("borderTopStyle: "     + computedStyle.getProperty("borderTopStyle")  + "<br>");
		sb.append("borderColor: "        + computedStyle.getProperty("borderColor")  + "<br>");  // shorthand property, does NOT work for reading
		sb.append("borderStyle: "        + computedStyle.getProperty("borderStyle")  + "<br>");  // shorthand property, does NOT work for reading 
		
		sb.append("borderTop: "     + computedStyle.getIntPixelProperty("borderTopWidth") + "<br>");
		sb.append("borderLeft: "    + computedStyle.getIntPixelProperty("borderLeftWidth") + "<br>");
		sb.append("borderRight: "   + computedStyle.getIntPixelProperty("borderRightWidth") + "<br>");
		sb.append("borderBottom: "  + computedStyle.getIntPixelProperty("borderBottomWidth") + "<br>");
		sb.append("paddingTop: "    + computedStyle.getIntPixelProperty("paddingTop") + "<br>");
		sb.append("paddingLeft: "   + computedStyle.getIntPixelProperty("paddingLeft") + "<br>");
		sb.append("paddingRight: "  + computedStyle.getIntPixelProperty("paddingRight") + "<br>");
		sb.append("paddingBottom: " + computedStyle.getIntPixelProperty("paddingBottom") + "<br>");
		
		sb.append("isVisible: " + w.isVisible() + "<br>");
		
		return sb.toString();
	}
}
