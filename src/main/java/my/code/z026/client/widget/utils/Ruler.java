package my.code.z026.client.widget.utils;

import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Label;

public class Ruler extends AbsolutePanel {

	protected Ruler(int baseLeft, int baseTop, int length, Orientation orientation, RulerTemplate template) {
		super();
		Style s = getElement().getStyle();
		s.setPosition(Position.ABSOLUTE);
		s.setTop(baseTop + orientation.topOffset(template.space, template.rulerHeight), Unit.PX);
		s.setLeft(baseLeft + orientation.leftOffset(template.space, template.rulerHeight), Unit.PX);
		s.setWidth(orientation.getWidth(length, template.rulerHeight), Unit.PX);
		s.setHeight(orientation.getHeight(length, template.rulerHeight), Unit.PX);
		s.setTextAlign(TextAlign.CENTER);
		s.setOverflow(Overflow.VISIBLE);
		setStyleName(template.style);
		Label label = new RotableLabel(length + "&nbsp;px", orientation.isVertical());
		add(label);
		setWidgetPosition(label, orientation.labelXOffset(length), orientation.labelYOffset(length));
	}

	public static class RulerTemplate {
		private final int space;
		private final int rulerHeight;
		private final String style;

		public RulerTemplate(int space, int rulerHeight, String style) {
			super();
			this.space = space;
			this.rulerHeight = rulerHeight;
			this.style = style;
		}

		public Ruler apply(int baseLeft, int baseTop, int length, Orientation orientation) {
			return new Ruler(baseLeft, baseTop, length, orientation, this);
		}
	}

	public static enum Orientation {
		ABOVE(0, 0, -1, -1, 1, 0),
		LEFT(-1, -1, 0, 0, 0, 1);

		int kx1, kx2, ky1, ky2, kw, kh;

		private Orientation(int kx1, int kx2, int ky1, int ky2, int kw, int kh) {
			this.kx1 = kx1;
			this.kx2 = kx2;
			this.ky1 = ky1;
			this.ky2 = ky2;
			this.kw = kw;
			this.kh = kh;
		}

		public int leftOffset(int spaceBetween, int rulerHeight) {
			return kx1 * spaceBetween + kx2 * rulerHeight;
		}

		public int topOffset(int spaceBetween, int rulerHeight) {
			return ky1 * spaceBetween + ky2 * rulerHeight;
		}

		public int getWidth(int rulerWidth, int rulerHeight) {
			return kw * rulerWidth + kh * rulerHeight;
		}

		public int getHeight(int rulerWidth, int rulerHeight) {
			return kh * rulerWidth + kw * rulerHeight;
		}

		public boolean isVertical() {
			return (kw == 0 && kh == 1);
		}
		
		public int labelXOffset(int length) {
			return 1;
		}
		
		public int labelYOffset(int length) {
			return kh * (length - 2) + 1;
		}
	}

}
