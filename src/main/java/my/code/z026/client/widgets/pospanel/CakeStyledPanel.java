package my.code.z026.client.widgets.pospanel;

import my.code.z026.client.widget.utils.ComputedStyle;

import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Positionable panel. Get around problems with padding and border using one inner panel. This
 * implementation uses 3 panels, layered panels, hence the 'cake'. As an improvement to PosCakePanel
 * the border and padding can be set via CSS.
 * 
 * OuterPanel - 0px border, 0px padding (explicitly stated in the code, overriding any influence
 * from CSS) This panel is used to position the panel. Set top,left, height, width is called
 * exclusively on this panel. Since border and padding are zero, then offsetHeight == height and
 * offsetWidth == width. setHeight() and setWidth() work as intuitively expected.
 * 
 * InnerPanel - to define "border" (effect similar to border) Style is explicitly set as 0px border,
 * 0px padding, in code, overriding any influence from CSS.
 * 
 * InnerInnerPanel - to define "padding" (effect similar to padding) Style is explicitly set as 0px
 * border, 0px padding, in code, overriding any influence from CSS.
 * 
 * HiddenStylePanel - invisible panel with a only one special function. Panel style is applied to it
 * like to innerPanel, padding and border is then read from this element computedStyle. It cannot be
 * read from innerPanel because we have to overwrite padding and border.
 * 
 * Note: border and background properties are applied to the inner panel
 * 
 * @author espinosa
 */
public class CakeStyledPanel extends AbsolutePanel {

	private final AbsolutePanel innerPanel;
	private final AbsolutePanel innerInnerPanel;
	private final AbsolutePanel hiddenStylePanel;

	private ComputedStyle computedStyle;

	private int borderTop = 0;
	private int borderLeft = 0;
	private int borderRight = 0;
	private int borderBottom = 0;

	private int paddingTop = 0;
	private int paddingLeft = 0;
	private int paddingRight = 0;
	private int paddingBottom = 0;

	public CakeStyledPanel() {
		super();

		innerPanel = new AbsolutePanel();
		innerInnerPanel = new AbsolutePanel();
		hiddenStylePanel = new AbsolutePanel();

		Style s = this.getElement().getStyle();
		s.setBorderWidth(0, Unit.PX);
		s.setPadding(0, Unit.PX);
		s.setPosition(Position.ABSOLUTE);

		innerPanel.getElement().getStyle().setLeft(0, Unit.PX);
		innerPanel.getElement().getStyle().setTop(0, Unit.PX);
		innerPanel.getElement().getStyle().setRight(0, Unit.PX);
		innerPanel.getElement().getStyle().setBottom(0, Unit.PX);
		innerPanel.getElement().getStyle().setBorderWidth(0, Unit.PX); //..we can set border size in CSS, and color and style
		innerPanel.getElement().getStyle().setPadding(0, Unit.PX);
		innerPanel.getElement().getStyle().setPosition(Position.ABSOLUTE);

		innerInnerPanel.getElement().getStyle().setLeft(0, Unit.PX);
		innerInnerPanel.getElement().getStyle().setTop(0, Unit.PX);
		innerInnerPanel.getElement().getStyle().setRight(0, Unit.PX);
		innerInnerPanel.getElement().getStyle().setBottom(0, Unit.PX);
		innerInnerPanel.getElement().getStyle().setBorderWidth(0, Unit.PX);
		innerInnerPanel.getElement().getStyle().setPadding(0, Unit.PX);
		innerInnerPanel.getElement().getStyle().setPosition(Position.ABSOLUTE);

		hiddenStylePanel.getElement().getStyle().setLeft(0, Unit.PX);
		hiddenStylePanel.getElement().getStyle().setTop(0, Unit.PX);
		hiddenStylePanel.getElement().getStyle().setRight(0, Unit.PX);
		hiddenStylePanel.getElement().getStyle().setBottom(0, Unit.PX);
		hiddenStylePanel.getElement().getStyle().setPosition(Position.ABSOLUTE);
		hiddenStylePanel.setVisible(true);

		super.add(hiddenStylePanel);
		super.add(innerPanel);
		innerPanel.add(innerInnerPanel);
	}

	private void setBorderAndPaddingFromStyle() {
		computedStyle = new ComputedStyle(hiddenStylePanel);
		setBorderTop(computedStyle.getIntPixelProperty("borderTopWidth"));
		setBorderLeft(computedStyle.getIntPixelProperty("borderLeftWidth"));
		setBorderRight(computedStyle.getIntPixelProperty("borderRightWidth"));
		setBorderBottom(computedStyle.getIntPixelProperty("borderBottomWidth"));
		setPaddingTop(computedStyle.getIntPixelProperty("paddingTop"));
		setPaddingLeft(computedStyle.getIntPixelProperty("paddingLeft"));
		setPaddingRight(computedStyle.getIntPixelProperty("paddingRight"));
		setPaddingBottom(computedStyle.getIntPixelProperty("paddingBottom"));
	}

	public Panel getPanel() {
		return innerInnerPanel;
	}

	/**
	 * Override style bearing element method. Change it to innerPannel instead of 'this'
	 */
	@Override
	protected com.google.gwt.user.client.Element getStyleElement() {
		return innerPanel.getElement();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setStyleName(String style) {
		super.setStyleName(style);
		hiddenStylePanel.setStyleName(style);
		setBorderAndPaddingFromStyle();
	}

	@Override
	public void add(Widget w) {
		innerInnerPanel.add(w);
	}

	public void setWidth(int width) {
		getElement().getStyle().setWidth(width, Unit.PX);
	}

	public int getWidth() {
		return this.getOffsetWidth();
	}

	public void setHeight(int height) {
		getElement().getStyle().setHeight(height, Unit.PX);
	}

	public int getHeight() {
		return this.getOffsetHeight();
	}

	public int getLeft() {
		return DOM.getElementPropertyInt(getElement(), "style.left");
	}

	public void setLeft(int left) {
		getElement().getStyle().setLeft(left, Unit.PX);
	}

	public int getTop() {
		return DOM.getElementPropertyInt(getElement(), "style.top");
	}

	public void setTop(int top) {
		getElement().getStyle().setTop(top, Unit.PX);
	}

	/**
	 * Helper method setting position at once
	 * 
	 * @param left
	 * @param top
	 * @param width
	 * @param height
	 */
	public void setPixelSize(int left, int top, int width, int height) {
		setLeft(left);
		setTop(top);
		setWidth(width);
		setHeight(height);
	}

	public void setBorderWidth(int borderWidth) {
		setBorderTop(borderWidth);
		setBorderLeft(borderWidth);
		setBorderRight(borderWidth);
		setBorderBottom(borderWidth);
	}

	public void setPaddingWidth(int paddingWidth) {
		setPaddingTop(paddingWidth);
		setPaddingLeft(paddingWidth);
		setPaddingRight(paddingWidth);
		setPaddingBottom(paddingWidth);
	}

	public int getBorderTop() {
		return borderTop;
	}

	public void setBorderTop(int borderTop) {
		this.borderTop = borderTop;
		innerPanel.getElement().getStyle().setProperty("borderTopWidth", borderTop + "px");
	}

	public int getBorderLeft() {
		return borderLeft;
	}

	public void setBorderLeft(int borderLeft) {
		this.borderLeft = borderLeft;
		innerPanel.getElement().getStyle().setProperty("borderLeftWidth", borderLeft + "px");
	}

	public int getBorderRight() {
		return borderRight;
	}

	public void setBorderRight(int borderRight) {
		this.borderRight = borderRight;
		innerPanel.getElement().getStyle().setProperty("borderRightWidth", borderRight + "px");
	}

	public int getBorderBottom() {
		return borderBottom;
	}

	public void setBorderBottom(int borderBottom) {
		this.borderBottom = borderBottom;
		innerPanel.getElement().getStyle().setProperty("borderBottomWidth", borderBottom + "px");
	}

	public int getPaddingTop() {
		return paddingTop;
	}

	public void setPaddingTop(int paddingTop) {
		this.paddingTop = paddingTop;
		innerInnerPanel.getElement().getStyle().setMarginTop(paddingTop, Unit.PX);
	}

	public int getPaddingLeft() {
		return paddingLeft;
	}

	public void setPaddingLeft(int paddingLeft) {
		this.paddingLeft = paddingLeft;
		innerInnerPanel.getElement().getStyle().setMarginLeft(paddingLeft, Unit.PX);
	}

	public int getPaddingRight() {
		return paddingRight;
	}

	public void setPaddingRight(int paddingRight) {
		this.paddingRight = paddingRight;
		innerInnerPanel.getElement().getStyle().setMarginRight(paddingRight, Unit.PX);
	}

	public int getPaddingBottom() {
		return paddingBottom;
	}

	public void setPaddingBottom(int paddingBottom) {
		this.paddingBottom = paddingBottom;
		innerInnerPanel.getElement().getStyle().setMarginBottom(paddingBottom, Unit.PX);
	}
}
