package my.code.z026.client.widgets.pospanel;

import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.AbsolutePanel;

/**
 * Positionable panel. Get around problems with padding and border using one inner panel.
 * This implementation uses 3 panels, layered panels, hence the 'cake'.
 * See also issues with PosAPanel.
 * 
 * Limitations: padding and border size are set programmatically, not via CSS. CSS values are ignored.
 * 
 * OuterPanel - 0px border, 0px padding (explicitly stated in the code, overriding any influence from CSS)
 * This panel is used to position the panel. Set top,left, height, width is called exclusively on this panel.
 * Since border and padding are zero, then offsetHeight == height and offsetWidth == width.
 * setHeight() and setWidth() work as intuitively expected.
 * 
 * InnerPanel - to define "border" (effect similar to border)
 * Style is explicitly set as 0px border, 0px padding, in code, overriding any influence from CSS.
 * 
 * InnerInnerPanel - to define "padding" (effect similar to padding)
 * Style is explicitly set as 0px border, 0px padding, in code, overriding any influence from CSS.
 * 
 * Note: border and background properties are applied to the inner panel
 * 
 * @author espinosa
 * @since 26.6.2013
 */
public class BoxPanel extends AbsolutePanel {

	public BoxPanel() {
		super();
		Style s = this.getElement().getStyle();
		s.setPosition(Position.ABSOLUTE);
		s.setOverflow(Overflow.HIDDEN);
		s.setProperty("MozBoxSizing", "border-box"); // -moz-box-sizing: border-box;
		s.setProperty("boxSizing", "border-box"); // box-sizing: border-box;
	}

	public void setWidth(int width) {
		getElement().getStyle().setWidth(width, Unit.PX);
	}

	public int getWidth() {
		return this.getOffsetWidth();
	}

	public void setHeight(int height) {
		getElement().getStyle().setHeight(height, Unit.PX);
	}

	public int getHeight() {
		return this.getOffsetHeight();
	}

	public int getLeft() {
		return DOM.getElementPropertyInt(getElement(), "style.left");
	}

	public void setLeft(int left) {
		getElement().getStyle().setLeft(left, Unit.PX);
	}

	public int getTop() {
		return DOM.getElementPropertyInt(getElement(), "style.top");
	}

	public void setTop(int top) {
		getElement().getStyle().setTop(top, Unit.PX);
	}

	/**
	 * Helper method setting position at once
	 * @param left
	 * @param top
	 * @param width
	 * @param height
	 */
	public void setPixelSize(int left, int top, int width, int height) {
		setLeft(left);
		setTop(top);
		setWidth(width);
		setHeight(height);
	}
}
