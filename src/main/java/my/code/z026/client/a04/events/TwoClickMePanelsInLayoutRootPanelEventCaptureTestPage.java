package my.code.z026.client.a04.events;

import java.util.logging.Logger;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.logging.client.HasWidgetsLogHandler;
import com.google.gwt.logging.client.LoggingPopup;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootLayoutPanel;

/**
 * Test event capturing. Create two panels, explicitly set their position side
 * by side, inside RootLayoutPanel and check that both react on mouse clicking.
 * Test Event capture. Log every click. See
 * {@link TwoClickMePanelsEventCaptureTestPage} for more details.
 * <p>
 * These Note the different positioning methods, using style top/left on
 * children panels does not work at all, setting style margin top/left has
 * desired visual effect, both panels are positioned side by side, but but only
 * the last added captures any events. In our case only panel2 logs any clicks,
 * panel1 is mute.
 * 
 * @author espinosa 31.10.2012, 1.11., 2.11.
 * 
 */
public class TwoClickMePanelsInLayoutRootPanelEventCaptureTestPage implements EntryPoint {

	public final Logger logger = Logger.getLogger("test");
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		final RootLayoutPanel rootPanel = RootLayoutPanel.get();
		
		TestPanel panel1 = new TestPanel("test-panel-1");
		panel1.add(new HTML("Test panel 1<br>click me!"));	
		rootPanel.add(panel1);
		panel1.addStyleName("blue-box");
//		panel1.setWidth(200);
//		panel1.setHeight(200);
//		panel1.setTop(10);
//		panel1.setLeft(10);
		rootPanel.setWidgetLeftWidth(panel1, 10, Unit.PX, 200, Unit.PX);
		rootPanel.setWidgetTopHeight(panel1, 10, Unit.PX, 200, Unit.PX);
		
		TestPanel panel2 = new TestPanel("test-panel-2");
		panel2.add(new HTML("Test panel 2<br>click me!"));	
		rootPanel.add(panel2);
		panel2.addStyleName("blue-box");
//		panel2.setWidth(200);
//		panel2.setHeight(200);
//		panel2.setTop(10);
//		panel2.setLeft(300);
		rootPanel.setWidgetLeftWidth(panel2, 300, Unit.PX, 200, Unit.PX);
		rootPanel.setWidgetTopHeight(panel2, 10, Unit.PX, 200, Unit.PX);
		
		PopupPanel loggingPopup = new LoggingPopup();
		loggingPopup.setPopupPosition(10, 240);
		loggingPopup.setWidth("500px");
		logger.addHandler(new HasWidgetsLogHandler(loggingPopup));
	}
	
	public class TestPanel extends AbsolutePanel {
		private final String widgetId;

		public TestPanel(String widgetId) {
			super();
			sinkEvents(
					Event.ONCLICK
					);	
			this.widgetId = widgetId;
			this.getElement().getStyle().setPosition(Position.ABSOLUTE);
		}

		@Override
		public void onBrowserEvent(Event event) {
			System.out.println("onBrowserEvent() " + widgetId + ", event:" + event.getType());
			super.onBrowserEvent(event);
		}

		public void setWidth(int width) {
			super.setWidth(width + "px");
		}

		public void setHeight(int height) {
			super.setHeight(height + "px");
		}

		public void setLeft(int left) {
			getElement().getStyle().setMarginLeft(left, Unit.PX);
		}

		public void setTop(int top) {
			getElement().getStyle().setMarginTop(top, Unit.PX);
		}
	}
	
	public static void main(String[] args) {
		my.code.z026.util.RunnableEntryPoint.run(TwoClickMePanelsEventCaptureTestPage.class);
	}
}
