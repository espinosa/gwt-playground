package my.code.z026.client.a04.events;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.logging.client.HasWidgetsLogHandler;
import com.google.gwt.logging.client.LoggingPopup;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Test event capturing. Create two panels, explicitly set their position side
 * by side, and check that both react on mouse clicking. Test Event capture. Log
 * every click. Expected output:
 * <pre>
 * onBrowserEvent() test-panel-1, event:click
 * onBrowserEvent() test-panel-2, event:click
 * </pre>
 * <p>
 * Main reason for creating this test was my original implementation of
 * positionable panel based on setting top and left margins, via its Style, to
 * position it inside the parent element. Visually, it looked right, but the
 * first added panel failed to capture any Event, like a basic mouse click
 * event. The second added had no problem in capturing any Events.
 * <p>
 * The problem was solved by switching from RootLayoutPanel to RootPanel. In
 * another words, by changing parent from LayoutPanel to AbsolutePanel. When
 * employing LayoutPanel as parent container, setting style top and left
 * attributes does not work, it is blocked by GWT (?), not reflected in the DOM.
 * Setting style top/left margins works and has desired visual effect , but this
 * solution has side effects when it comes to Event capturing.
 * <p>
 * In the parent container is a LayoutPanel, all children parent are wrapped by
 * an extra DIV - they Layer DIV. The Layer wrapper DIV is strangely stretched
 * to 100% of parent container, it is transparent, so it is not observable, in
 * effect blocks any Event, like mouse click, for everything but the last added
 * child panel. To observe this issue you need add at least two child panels.
 * 
 * @author espinosa 31.10.2012, 1.11., 2.11.
 * 
 */
public class TwoClickMePanelsEventCaptureTestPage implements EntryPoint {
	public final Logger logger = Logger.getLogger("test");


	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		final RootPanel rootPanel = RootPanel.get();
		
		TestPanel panel1 = new TestPanel("test-panel-1");
		panel1.add(new HTML("Test panel 1<br>click me!"));	
		rootPanel.add(panel1);
		panel1.addStyleName("blue-box");
		panel1.setWidth(200);
		panel1.setHeight(200);
		panel1.setTop(10);
		panel1.setLeft(10);
		
		TestPanel panel2 = new TestPanel("test-panel-2");
		panel2.add(new HTML("Test panel 2<br>click me!"));	
		rootPanel.add(panel2);
		panel2.addStyleName("blue-box");
		panel2.setWidth(200);
		panel2.setHeight(200);
		panel2.setTop(10);
		panel2.setLeft(300);
		
		PopupPanel loggingPopup = new LoggingPopup();
		loggingPopup.setPopupPosition(10, 240);
		loggingPopup.setWidth("500px");
		logger.addHandler(new HasWidgetsLogHandler(loggingPopup));
	}
	
	public class TestPanel extends AbsolutePanel {
		private final String widgetId;

		public TestPanel(String widgetId) {
			super();
			sinkEvents(
					Event.ONCLICK
					);	
			this.widgetId = widgetId;
			this.getElement().getStyle().setPosition(Position.ABSOLUTE);
		}

		@Override
		public void onBrowserEvent(Event event) {
			logger.log(Level.INFO, "onBrowserEvent() " + widgetId + ", event:" + event.getType());
			super.onBrowserEvent(event);
		}

		public void setWidth(int width) {
			super.setWidth(width + "px");
		}

		public void setHeight(int height) {
			super.setHeight(height + "px");
		}

		public void setLeft(int left) {
			getElement().getStyle().setLeft(left, Unit.PX);
		}

		public void setTop(int top) {
			getElement().getStyle().setTop(top, Unit.PX);
		}
	}
	
	public static void main(String[] args) {
		my.code.z026.util.RunnableEntryPoint.run(TwoClickMePanelsEventCaptureTestPage.class);
	}
}
