package my.code.z026.client.a03.positioning;

import my.code.z026.client.widget.utils.DummyContentProvider;
import my.code.z026.client.widgets.pospanel.CakeStyledPanel;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Test {@link CakeStyledPanel}. The {@link CakeStyledPanel} is extension of normal {@link Panel} where set
 * width and height represents visible edge, including padding and border.
 * <p>
 * Make composition of several {@link CakeStyledPanel} with different visual properties. Also demonstrate
 * that {@link CakeStyledPanel} is capable of sizes smaller then padding and border. Test it on 2 small
 * panels, small as 10x10 and even 1x1 pixels.
 */
public class A04CakeStyledPanelTest implements EntryPoint {

	String panelContent = DummyContentProvider.getText();

	@Override
	public void onModuleLoad() {
		System.out.println("PosCake2PanelTestPage");
		
		final CakeStyledPanel panel1 = new CakeStyledPanel();
		RootPanel.get().add(panel1);
		panel1.add(new HTML(panelContent));
		panel1.setStyleName("main-panel");
		//panel1.setBorderWidth(1);  // this should be defined in CSS
		//panel1.setPaddingWidth(5); // this should be defined in CSS
		panel1.setPixelSize(0,0,100,100);

		final CakeStyledPanel panel2 = new CakeStyledPanel();
		RootPanel.get().add(panel2);
		panel2.setStyleName("main-panel2");
		panel2.setPixelSize(100,100,200,200);
		//panel2.setBorderWidth(5);  // this should be defined in CSS
		//panel2.setPaddingWidth(5); // this should be defined in CSS

		// 10x10 panel, panel 3 is inside panel 2
		final CakeStyledPanel panel3 = new CakeStyledPanel();
		panel2.add(panel3);
		panel3.setStyleName("main-panel3");
		panel3.setPixelSize(80,80,10,10);
		//panel3.setBorderWidth(1);
		//panel3.setPaddingWidth(5);

		// 1x1 panel, panel 4 is inside panel 2
		final CakeStyledPanel panel4 = new CakeStyledPanel();
		panel2.add(panel4);
		panel4.setStyleName("main-panel4");
		panel4.setPixelSize(10,10,1,1);
		//panel4.setBorderWidth(1);
		//panel4.setPaddingWidth(5);
	}
	
	public static void main(String[] args) {
		my.code.z026.util.RunnableEntryPoint.run(A04CakeStyledPanelTest.class);
	}
}
