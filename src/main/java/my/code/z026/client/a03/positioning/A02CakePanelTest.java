package my.code.z026.client.a03.positioning;

import my.code.z026.client.widget.utils.DummyContentProvider;
import my.code.z026.client.widgets.pospanel.CakePanel;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Test {@link CakePanel}. The {@link CakePanel} is extension of normal {@link Panel} where set
 * width and height represents visible edge, including padding and border.
 * <p>
 * Make composition of several {@link CakePanel} with different visual properties. Also demonstrate
 * that {@link CakePanel} is capable of sizes smaller then padding and border. Test it on 2 small
 * panels, small as 10x10 and even 1x1 pixels.
 */
public class A02CakePanelTest implements EntryPoint {

	String panelContent = DummyContentProvider.getText();

	@Override
	public void onModuleLoad() {
		final CakePanel panel1 = new CakePanel();
		panel1.setStyleName("main-panel");
		panel1.add(new HTML(panelContent));
		panel1.setBorderWidth(1);
		panel1.setPaddingWidth(5);
		panel1.setPixelSize(0, 0, 100, 100);

		final CakePanel panel2 = new CakePanel();
		panel2.setStyleName("main-panel2");
		panel2.setPixelSize(100, 100, 200, 200);
		panel2.setBorderWidth(5);
		panel2.setPaddingWidth(5);

		// 10x10 panel, panel 3 is inside panel 2
		final CakePanel panel3 = new CakePanel();
		panel3.setStyleName("main-panel3");
		panel3.setPixelSize(80, 80, 10, 10);
		panel3.setBorderWidth(1);
		panel3.setPaddingWidth(5);
		panel2.add(panel3);

		// 1x1 panel, panel 4 is inside panel 2
		final CakePanel panel4 = new CakePanel();
		panel4.setStyleName("main-panel4");
		panel4.setPixelSize(10, 10, 1, 1);
		panel4.setBorderWidth(1);
		panel4.setPaddingWidth(5);
		panel2.add(panel4);

		RootPanel.get().add(panel1);
		RootPanel.get().add(panel2);
	}

	public static void main(String[] args) {
		my.code.z026.util.RunnableEntryPoint.run(A02CakePanelTest.class);
	}
}
