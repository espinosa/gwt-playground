package my.code.z026.client.a03.positioning;

import my.code.z026.client.a01.positioning.A29ShowBottomPaddingOverflowIssue;
import my.code.z026.client.widget.utils.DummyContentProvider;
import my.code.z026.client.widgets.pospanel.BoxPanel;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Test {@link BoxPanel}. The {@link BoxPanel} is extension of normal {@link Panel} where set width
 * and height represents visible edge, including padding and border.
 * <p>
 * Make composition of several {@link BoxPanel} with different visual properties. Also demonstrate
 * that {@link BoxPanel} is capable of sizes smaller then padding and border. Test it on 2 small
 * panels, small as 10x10 and even 1x1 pixels.
 * <p>
 * FIXME: {@link BoxPanel} does fails in this test. It cannot shape itself below size of 2 * padding
 * + 2 * border. 
 * <p>
 * Also there is known issue with text overflowing to bottom padding. See {@link A29ShowBottomPaddingOverflowIssue}. 
 */
public class A06BoxPanelTest implements EntryPoint {

	String panelContent = DummyContentProvider.getText();

	@Override
	public void onModuleLoad() {
		System.out.println("PosCake2PanelTestPage");

		final BoxPanel panel1 = new BoxPanel();
		RootPanel.get().add(panel1);
		panel1.add(new HTML(panelContent));
		panel1.setStyleName("main-panel");
		panel1.setPixelSize(0, 0, 100, 100);

		final BoxPanel panel2 = new BoxPanel();
		RootPanel.get().add(panel2);
		panel2.setStyleName("main-panel2");
		panel2.setPixelSize(100, 100, 200, 200);

		// 10x10 panel, panel 3 is inside panel 2
		final BoxPanel panel3 = new BoxPanel();
		panel2.add(panel3);
		panel3.setStyleName("main-panel3");
		panel3.setPixelSize(80, 80, 10, 10);

		// 1x1 panel, panel 4 is inside panel 2
		final BoxPanel panel4 = new BoxPanel();
		panel2.add(panel4);
		panel4.setStyleName("main-panel4");
		panel4.setPixelSize(10, 10, 1, 1);
	}

	public static void main(String[] args) {
		my.code.z026.util.RunnableEntryPoint.run(A06BoxPanelTest.class);
	}
}
