package my.code.z026.client.a03.positioning;

import my.code.z026.client.widget.utils.InfoPanel.InfoPanelTemplate;
import my.code.z026.client.widget.utils.PositionIntrospector;
import my.code.z026.client.widget.utils.Ruler.Orientation;
import my.code.z026.client.widget.utils.Ruler.RulerTemplate;
import my.code.z026.client.widgets.pospanel.BoxPanel;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Test {@link BoxPanel}. The {@link BoxPanel} is extension of normal {@link Panel} where set width
 * and height represents visible edge, including padding and border. Draw rulers to compare visually
 * tested width and height with desired values. Display info box with relevant position and shape
 * properties of the tested panel.
 */
public class A05BoxPanelTest implements EntryPoint {

	private static final String panelContent = ""
		+ "<b>Test panel</b><br>"
		+ "set to have width: 200px and height: 200px<br>"
		+ "But it also has padding 20px and border 1px thick.";

	private final RulerTemplate rt = new RulerTemplate(5, 20, "ruler");

	private final InfoPanelTemplate ipt = new InfoPanelTemplate("infobox", 200);

	private final PositionIntrospector pi = new PositionIntrospector();

	@Override
	public void onModuleLoad() {
		RootPanel rootPanel = RootPanel.get();

		final BoxPanel testedPanel = new BoxPanel();
		rootPanel.add(testedPanel);

		testedPanel.setStyleName("main-panel");
		testedPanel.add(new HTML(panelContent));
		testedPanel.setLeft(100);
		testedPanel.setTop(100);
		testedPanel.setPixelSize(200, 200);

		rootPanel.add(rt.apply(100, 100, 200, Orientation.ABOVE));
		rootPanel.add(rt.apply(100, 100, 200, Orientation.LEFT));
		rootPanel.add(ipt.newPanel(310, 310, pi.getProperties(testedPanel), "Properties of Test panel"));
	}

	public static void main(String[] args) {
		my.code.z026.util.RunnableEntryPoint.run(A05BoxPanelTest.class);
	}
}
