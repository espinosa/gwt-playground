package my.code.z026.client.a01.positioning;

import my.code.z026.client.widget.utils.DummyContentProvider;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Draw panel with zero margins (margin-left; 0px, margin-right:0px; ..) and no other dimensions
 * set. The panel then should fill full space of its parent - the root panel. If the root panel is
 * RootLayoutPanel then it should fill full browser window.
 */
public class A21ZeroMarginPanelWithRootPanel implements EntryPoint {

	private static final String panelContent = DummyContentProvider.getText();

	@Override
	public void onModuleLoad() {
		RootPanel rootPanel = RootPanel.get();
		
		final AbsolutePanel panel = new AbsolutePanel();
		panel.setStyleName("main-panel");
		panel.add(new HTML(panelContent));

		Style s = panel.getElement().getStyle();
		s.setMarginLeft(1, Unit.PCT);
		s.setMarginTop(1, Unit.PCT);
		s.setMarginRight(1, Unit.PCT);
		s.setMarginBottom(1, Unit.PCT);
		s.setPosition(Position.ABSOLUTE);

		rootPanel.add(panel);
	}

	public static void main(String[] args) {
		my.code.z026.util.RunnableEntryPoint.run(A21ZeroMarginPanelWithRootPanel.class);
	}
}
