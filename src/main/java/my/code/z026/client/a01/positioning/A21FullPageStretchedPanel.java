package my.code.z026.client.a01.positioning;

import my.code.z026.client.widget.utils.DummyContentProvider;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Draw panel with covering full page viewport - 100% height and 100% width of browser window client
 * area.
 * <p>
 * Demonstrate scrollbar issue. For no good reason both vertical and horizontal scrollbars appear.
 */
public class A21FullPageStretchedPanel implements EntryPoint {

	private static final String panelContent = DummyContentProvider.getText();

	@Override
	public void onModuleLoad() {
		RootPanel rootPanel = RootPanel.get();

		final AbsolutePanel panel = new AbsolutePanel();
		panel.setStyleName("main-panel");
		panel.add(new HTML(panelContent));

		Style s = panel.getElement().getStyle();
		s.setPosition(Position.ABSOLUTE);
		s.setLeft(0, Unit.PX);
		s.setTop(0, Unit.PX);
		s.setHeight(100, Unit.PCT);
		s.setWidth(100, Unit.PCT);

		rootPanel.add(panel);
	}

	public static void main(String[] args) {
		my.code.z026.util.RunnableEntryPoint.run(A21FullPageStretchedPanel.class);
	}
}
