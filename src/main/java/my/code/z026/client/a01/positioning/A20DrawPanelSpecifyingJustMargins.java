package my.code.z026.client.a01.positioning;

import my.code.z026.client.widget.utils.DummyContentProvider;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootLayoutPanel;

/**
 * Draw panel with 20px margins (margin-left; 20px, margin-right: 20px; ..) and no other dimensions
 * set. The panel then should fill full space of its parent - the root panel.
 * <p>
 * This should draw a panel stretching most of the browser page client area, except 20px wide frame
 * around, resizing with it, because bottom and right side of the tested panel are now bonded to
 * parent (browser window) shape.
 */
public class A20DrawPanelSpecifyingJustMargins implements EntryPoint {

	private static final String panelContent = DummyContentProvider.getText();

	@Override
	public void onModuleLoad() {
		RootLayoutPanel rootPanel = RootLayoutPanel.get();

		final AbsolutePanel panel = new AbsolutePanel();
		panel.setStyleName("main-panel");
		panel.add(new HTML(panelContent));

		Style s = panel.getElement().getStyle();
		s.setMarginLeft(20, Unit.PX);
		s.setMarginTop(20, Unit.PX);
		s.setMarginRight(20, Unit.PX);
		s.setMarginBottom(20, Unit.PX);

		rootPanel.add(panel);
	}

	public static void main(String[] args) {
		my.code.z026.util.RunnableEntryPoint.run(A20DrawPanelSpecifyingJustMargins.class);
	}
}
