package my.code.z026.client.a01.positioning;

import my.code.z026.client.widget.utils.DummyContentProvider;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Draw panel with covering full page viewport - 100% height and 100% width of browser window client
 * area.
 * <p>
 * Demonstrate scrollbar issue. For no good reason both vertical and horizontal scrollbars appear.
 */
public class A22FullPageStretchedPanelDynamically implements EntryPoint {

	private static final String panelContent = DummyContentProvider.getText();

	@Override
	public void onModuleLoad() {
		RootPanel rootPanel = RootPanel.get();

		final AbsolutePanel panel = new AbsolutePanel();
		panel.setStyleName("no-border-no-padding-gray");
		panel.add(new HTML(panelContent));

		rootPanel.add(panel);
		stretchPanelToFullPageViewport(panel);
		
		Window.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(ResizeEvent event) {
				stretchPanelToFullPageViewport(panel);
			}
		});
	}

	public void stretchPanelToFullPageViewport(AbsolutePanel panel) {
		int screenWidth = Window.getClientWidth();
		int screenHeight = Window.getClientHeight();		
		Style s = panel.getElement().getStyle();
		s.setPosition(Position.ABSOLUTE);
		s.setLeft(0, Unit.PX);
		s.setTop(0, Unit.PX);
		s.setHeight(screenHeight, Unit.PX);
		s.setWidth(screenWidth, Unit.PX);
	}

	public static void main(String[] args) {
		my.code.z026.util.RunnableEntryPoint.run(A22FullPageStretchedPanelDynamically.class);
	}
}
