package my.code.z026.client.a01.positioning;

import my.code.z026.client.widget.utils.ComputedStyle;
import my.code.z026.client.widget.utils.ComputedStyleInrospector;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Test that {@link ComputedStyle} methods works.
 * Show an ordinary Panels with CSS properties defined <b>in CSS</b>, not by code. Properties like border, padding, margin, left, top, background color, etc..
 * Check if the printout if it contains 
 * <p> 
 * Second Panel is similar, but invisible, to check if it still (should!) hold visible properties with it. 
 */
public class A10ShowComputedStyleForPanel implements EntryPoint {
	
	 private ComputedStyleInrospector csi = new ComputedStyleInrospector(); 

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		final AbsolutePanel testPanel = new AbsolutePanel();
		final AbsolutePanel printoutPanel1 = new AbsolutePanel();

		AbsolutePanel rootPanel = RootPanel.get();
		rootPanel.add(testPanel);
		rootPanel.add(printoutPanel1);

		rootPanel.setWidgetPosition(testPanel, 10, 10);
		rootPanel.setWidgetPosition(printoutPanel1, 10, 150);
		
		// Visible panel properties
		// Most properties are set by CSS styles, deliberately, this test is about reading them, reading of effective property values 
		testPanel.setStyleName("main-panel2");
		testPanel.setPixelSize(100,100);
		testPanel.setVisible(true);
		
		// print properties for visible panel
		printoutPanel1.setStyleName("infobox");
		printoutPanel1.add(new HTML(csi.getSomeComputedStyleProperties(testPanel, "Visible panel")));
	}
	
	/**
	 * Convenience method for easy launching of individual Entry Point. It does some checks and
	 * conveniently opens web browser on the right URL. In client code it is effectively ignored.
	 */
	public static void main(String[] args) {
		my.code.z026.util.RunnableEntryPoint.run(A10ShowComputedStyleForPanel.class);
	}
}
