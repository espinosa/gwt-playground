package my.code.z026.client.a01.positioning;

import my.code.z026.client.widget.utils.DummyContentProvider;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Simplified version with non centered static, non-resizable panel in the middle of the page. Made
 * to test setting absolute position for a widget.
 */
public class A01DynamicallyCenteredPanelHugePaddingTest implements EntryPoint {

	String panelContent = DummyContentProvider.getText();

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		final AbsolutePanel panel = new AbsolutePanel();
		panel.setPixelSize(200, 200);
		panel.setStyleName("main-panel");
		panel.add(new HTML(panelContent));
		
		panel.getElement().getStyle().setPadding(200, Unit.PX);
		
		RootPanel.get().add(panel);
		centerComponent(panel, (AbsolutePanel) panel.getParent());

		Window.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(ResizeEvent event) {
				centerComponent(panel, (AbsolutePanel) panel.getParent());
			}
		});
	}

	public void centerComponent(AbsolutePanel panel, AbsolutePanel parent) {
		int screenWidth = Window.getClientWidth();
		int screenHeight = Window.getClientHeight();
		int width = panel.getOffsetWidth();  // returns real width, including border and padding, so the centring computation is OK
		int height = panel.getOffsetHeight(); // returns real height, including border and padding, so the centring computation is OK
		int left = (screenWidth - width) / 2;
		int top = (screenHeight - height) / 2;
		parent.setWidgetPosition(panel, left, top);
	}

	/**
	 * Convenience method for easy launching of individual Entry Point. It does some checks and
	 * conveniently opens web browser on the right URL. In client code it is effectively ignored.
	 */
	public static void main(String[] args) {
		my.code.z026.util.RunnableEntryPoint.run(A01DynamicallyCenteredPanelHugePaddingTest.class);
	}
}
