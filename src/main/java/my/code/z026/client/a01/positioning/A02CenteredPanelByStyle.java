package my.code.z026.client.a01.positioning;

import java.util.logging.Logger;

import my.code.z026.client.widget.utils.DummyContentProvider;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Centre panel using CSS styles - by setting marginLeft and left, and martinTop and top.
 * Set styles by code (DOM operation) do not rely on any external CSS.
 */
public class A02CenteredPanelByStyle implements EntryPoint {

	private final String panelContent = DummyContentProvider.getText();
	
	public final Logger logger = Logger.getLogger("test");

	public void onModuleLoad() {
		RootPanel rootPanel = RootPanel.get();
		final AbsolutePanel panel = new AbsolutePanel();
		panel.setPixelSize(400, 400);
		panel.setStyleName("main-panel");
		panel.add(new HTML(panelContent));
		rootPanel.add(panel);
		centerComponent(panel);		
	}

	
	public void centerComponent(AbsolutePanel panel) {
		Style style = panel.getElement().getStyle();
		style.setPosition(Position.ABSOLUTE);
		
		// set left and top to 50% (relative measure) to body (parent element)
		// this alone sets top left corner of the panel to the middle of the screen
		style.setLeft(50, Unit.PCT);
		style.setTop(50, Unit.PCT);
		
		// Adjust the panel position by moving top left corner by half the width to the left and half of the panel height upwards
		// This way we get centre of panel on the centre of the page (body) 
		// We cannot use left/top (engaged in relative position to parent) but we can use margin, margin is relative to panel centre
		int panelWidth = panel.getOffsetWidth();
		int panelHeight = panel.getOffsetHeight();
		int halfPanelWidthInPx = panelWidth / 2;
		int halfPanelTopInPx = panelHeight / 2;
		style.setMarginLeft(-halfPanelWidthInPx, Unit.PX);
		style.setMarginTop(-halfPanelTopInPx, Unit.PX);
		
		// It re-centres automatically on browser window change
		// but must me explicitly re-centre with every change of panel shape (width/height)   
	}

	/**
	 * Convenience method for easy launching of individual Entry Point. It does some checks and
	 * conveniently opens web browser on the right URL. In client code it is effectively ignored.
	 */
	public static void main(String[] args) {
		my.code.z026.util.RunnableEntryPoint.run(A02CenteredPanelByStyle.class);
	}
}
