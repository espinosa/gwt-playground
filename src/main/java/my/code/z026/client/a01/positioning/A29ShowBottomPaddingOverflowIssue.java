package my.code.z026.client.a01.positioning;

import my.code.z026.client.widget.utils.DummyContentProvider;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Show issue with text (any content) overflowing to the bottom padding area.
 * http://stackoverflow.com/questions/8981811/overflowhidden-ignoring-bottom-padding
 * http://stackoverflow.com/questions/10722367/bottom-padding-not-working-on-overflow-element-in-non-chrome-browsers
 */
public class A29ShowBottomPaddingOverflowIssue implements EntryPoint {

	private static final String panelContent = DummyContentProvider.getText();

	@Override
	public void onModuleLoad() {
		RootPanel rootPanel = RootPanel.get();

		final AbsolutePanel panel = new AbsolutePanel();
		panel.setStyleName("main-panel");
		panel.add(new HTML(panelContent));
		Style style = panel.getElement().getStyle();
		style.setLeft(100, Unit.PX);
		style.setTop(100, Unit.PX);
		style.setWidth(200, Unit.PX);
		style.setHeight(200, Unit.PX);
		style.setPadding(20, Unit.PX);

		style.setOverflow(Overflow.HIDDEN);
		style.setOverflowY(Overflow.HIDDEN);
		
		rootPanel.add(panel);
	}

	public static void main(String[] args) {
		my.code.z026.util.RunnableEntryPoint.run(A29ShowBottomPaddingOverflowIssue.class);
	}
}
