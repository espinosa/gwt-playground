package my.code.z026.client.a01.positioning;

import my.code.z026.client.widget.utils.InfoPanel.InfoPanelTemplate;
import my.code.z026.client.widget.utils.PositionIntrospector;
import my.code.z026.client.widget.utils.Ruler.Orientation;
import my.code.z026.client.widget.utils.Ruler.RulerTemplate;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Show solution to issue with standard box sizing model. The size is set to 200px but in reality
 * the test box ends up being 212px sized. Padding (2x5px) and border (2 x 1px) are added to it.
 * Unless you change the box sizing to model border-sizing.
 * <p>
 * Demonstrate applied solution by drawing rulers around the tested panel and displaying info box
 * with position and shape information for the tested panel.
 * <p>
 * See: 
 * http://quirksmode.org/css/user-interface/boxsizing.html
 * https://developer.mozilla.org/en-US/docs/Web/CSS/box-sizing
 */
public class A31ShowIssueWithSettingPanelSizeFixByBorderBox implements EntryPoint {

	private static final String panelContent = ""
		+ "<b>Test panel</b><br>"
		+ "set to have width: 200px and height: 200px<br>"
		+ "But it also has padding 20px and border 1px thick.<br>"
		+ "With border box sizing, all is displayed ";

	private final RulerTemplate rt = new RulerTemplate(5, 20, "ruler");

	private final InfoPanelTemplate ipt = new InfoPanelTemplate("infobox", 200);

	private final PositionIntrospector pi = new PositionIntrospector();

	@Override
	public void onModuleLoad() {
		RootPanel rootPanel = RootPanel.get();

		final AbsolutePanel panel = new AbsolutePanel();
		panel.setStyleName("main-panel");
		panel.add(new HTML(panelContent));
		Style style = panel.getElement().getStyle();
		style.setLeft(100, Unit.PX);
		style.setTop(100, Unit.PX);
		style.setWidth(200, Unit.PX);
		style.setHeight(200, Unit.PX);
		style.setPadding(20, Unit.PX);
		style.setOverflow(Overflow.HIDDEN);

		style.setProperty("MozBoxSizing", "border-box"); // -moz-box-sizing: border-box;
		style.setProperty("boxSizing", "border-box"); // box-sizing: border-box;
		// default box sizing is "content-box"

		rootPanel.add(panel);
		rootPanel.add(rt.apply(100, 100, 200, Orientation.ABOVE));
		rootPanel.add(rt.apply(100, 100, 200, Orientation.LEFT));
		rootPanel.add(ipt.newPanel(350, 350, pi.getProperties(panel), "Properties of Test panel"));
	}

	public static void main(String[] args) {
		my.code.z026.util.RunnableEntryPoint.run(A31ShowIssueWithSettingPanelSizeFixByBorderBox.class);
	}
}
