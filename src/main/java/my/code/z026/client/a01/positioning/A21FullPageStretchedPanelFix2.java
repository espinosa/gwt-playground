package my.code.z026.client.a01.positioning;

import my.code.z026.client.widget.utils.DummyContentProvider;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Draw panel with covering full page viewport - 100% height and 100% width of browser window client
 * area.
 * <p>
 * The background panel must have no padding and border, then 100% of viewport size can be
 * effectively applied. The size (width or height) you set under standard box model is applied only
 * to content, inner, size. It the content panel have some border and/or padding, it is added to
 * 100% of size causing the panel being bigger then page viewport and scrollbars appear.
 */
public class A21FullPageStretchedPanelFix2 implements EntryPoint {

	private static final String panelContent = DummyContentProvider.getText();

	@Override
	public void onModuleLoad() {
		RootPanel rootPanel = RootPanel.get();

		final AbsolutePanel panel = new AbsolutePanel();
		panel.setStyleName("main-panel");
		panel.add(new HTML(panelContent));

		Style s = panel.getElement().getStyle();
		s.setPosition(Position.ABSOLUTE);
		s.setLeft(0, Unit.PX);
		s.setTop(0, Unit.PX);
		s.setHeight(100, Unit.PCT);
		s.setWidth(100, Unit.PCT);
		
		// https://developer.mozilla.org/en-US/docs/Web/CSS/box-sizing?redirectlocale=en-US&redirectslug=CSS%2Fbox-sizing
		// http://quirksmode.org/css/user-interface/boxsizing.html
		s.setProperty("boxSizing", "border-box");
		s.setProperty("MozBoxSizing", "border-box");

		rootPanel.add(panel);
	}

	public static void main(String[] args) {
		my.code.z026.util.RunnableEntryPoint.run(A21FullPageStretchedPanelFix2.class);
	}
}
