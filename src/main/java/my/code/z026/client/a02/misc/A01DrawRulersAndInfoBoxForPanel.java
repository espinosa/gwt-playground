package my.code.z026.client.a02.misc;

import my.code.z026.client.widget.utils.DummyContentProvider;
import my.code.z026.client.widget.utils.InfoPanel.InfoPanelTemplate;
import my.code.z026.client.widget.utils.PositionIntrospector;
import my.code.z026.client.widget.utils.Ruler.Orientation;
import my.code.z026.client.widget.utils.Ruler.RulerTemplate;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Draw panel surrounded with 2 rulers - one vertical, one horizontal with specified length. Draw
 * information box (another panel) with basic position and shape information about tested panel.
 * 
 * @since 26.6.2013
 */
public class A01DrawRulersAndInfoBoxForPanel implements EntryPoint {

	private static final String panelContent = DummyContentProvider.getText();

	private final RulerTemplate rt = new RulerTemplate(5, 20, "ruler");

	private final InfoPanelTemplate ipt = new InfoPanelTemplate("infobox", 200);

	private final PositionIntrospector pi = new PositionIntrospector();

	@Override
	public void onModuleLoad() {
		RootPanel rootPanel = RootPanel.get();

		final AbsolutePanel panel = new AbsolutePanel();
		panel.setStyleName("main-panel");
		panel.add(new HTML(panelContent));
		Style style = panel.getElement().getStyle();
		style.setLeft(100, Unit.PX);
		style.setTop(100, Unit.PX);
		style.setWidth(200, Unit.PX);
		style.setHeight(200, Unit.PX);
		style.setPadding(20, Unit.PX);

		style.setOverflow(Overflow.HIDDEN);

		rootPanel.add(panel);
		rootPanel.add(rt.apply(100, 100, 200, Orientation.ABOVE));
		rootPanel.add(rt.apply(100, 100, 200, Orientation.LEFT));
		rootPanel.add(ipt.newPanel(350, 350, pi.getProperties(panel), "Properties of Test panel"));
	}

	public static void main(String[] args) {
		my.code.z026.util.RunnableEntryPoint.run(A01DrawRulersAndInfoBoxForPanel.class);
	}
}
