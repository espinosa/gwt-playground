<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<!-- POM file generated with GWT webAppCreator -->
	<modelVersion>4.0.0</modelVersion>
	<groupId>my.code</groupId>
	<artifactId>z026-gwt-playground</artifactId>
	<packaging>war</packaging>
	<version>0.0.1-SNAPSHOT</version>
	<name>GWT Playground</name>

	<properties>
		<!-- Convenience property to set the GWT version -->
		<gwtVersion>2.5.1</gwtVersion>
		<!-- GWT needs at least java 1.5 -->
		<webappDirectory>${project.build.directory}/${project.build.finalName}</webappDirectory>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<dependencies>
		<dependency>
			<groupId>com.google.gwt</groupId>
			<artifactId>gwt-user</artifactId>
			<version>${gwtVersion}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<!-- Controversial option. Gwt-dev should not be in the classpath. It 
				contains potentially conflicting classes, like Apache IOUtils. The whole 
				library is generally a mess! But it is required for its Code Server classes 
				com.google.gwt.devDevMode, .. -->
			<groupId>com.google.gwt</groupId>
			<artifactId>gwt-dev</artifactId>
			<version>${gwtVersion}</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.7</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>javax.validation</groupId>
			<artifactId>validation-api</artifactId>
			<version>1.0.0.GA</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>javax.validation</groupId>
			<artifactId>validation-api</artifactId>
			<version>1.0.0.GA</version>
			<classifier>sources</classifier>
			<scope>provided</scope>
		</dependency>

		<!-- I had to downgrade Jetty from current 9.0.3 to 7.x.x. Higher versions 
			use Servlet 3.0 which conflicts with GWT; both gwt-dev and gwt-user have 
			their own copy of Servlet.class. What a mess! -->
		<dependency>
			<groupId>org.eclipse.jetty</groupId>
			<artifactId>jetty-server</artifactId>
			<version>7.6.7.v20120910</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.eclipse.jetty</groupId>
			<artifactId>jetty-servlet</artifactId>
			<version>7.6.7.v20120910</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<!-- Simple text replacing library working with streams -->
			<!-- It is used to modify application template html -->
			<groupId>org.codehaus.swizzle</groupId>
			<artifactId>swizzle-stream</artifactId>
			<version>1.6.2</version><!-- 1.6.1 has a bug! must be at least 1.6.2 version -->
		</dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>1.6.1</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
			<version>1.2.16</version>
			<scope>provided</scope>
		</dependency>
	</dependencies>

	<build>
		<!-- Generate compiled stuff in the folder used for developing mode -->
		<outputDirectory>${webappDirectory}/WEB-INF/classes</outputDirectory>

		<!-- GWT Code Server (DevMode) requires to have sources available in classpath. 
			One approach is to add source directories to classpath configuration, but 
			it is difficult (impossible) to make it via Maven+Eclipse. Other approach 
			is to simply copy client sources to directory with compiled classes. It creates 
			a bit mess with java files between class files in some directories but it 
			is bullet proof. Just don't forget skip this step for production code generation! -->
		<resources>
			<resource>
				<directory>src/main/java</directory>
				<filtering>false</filtering>
				<includes>
					<include>**/client/**</include>
					<include>**/overrides/**</include>
				</includes>
			</resource>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>false</filtering>
			</resource>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
				<includes>
					<include>application.properties</include>
				</includes>
			</resource>
			<resource>
				<directory>src/test/java</directory>
				<filtering>false</filtering>
				<includes>
					<include>**/client/**</include>
				</includes>
			</resource>
		</resources>

		<plugins>

			<!-- GWT Maven Plugin -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>gwt-maven-plugin</artifactId>
				<version>2.5.1</version>
				<configuration>
					<logLevel>INFO</logLevel><!-- DEBUG -->
					<style>DETAILED</style><!-- ?? -->
					<gwtVersion>${gwtVersion}</gwtVersion>
					<strict>true</strict>
				</configuration>
				<executions>
					<execution>
						<goals>
							<goal>compile</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- WAR plugin, used just to assemble web app directory -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<version>2.1.1</version>
				<configuration>
					<webappDirectory>${webappDirectory}</webappDirectory>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>2.3.2</version>
				<configuration>
					<source>1.6</source>
					<target>1.6</target>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<profiles>
		<profile>
			<id>default-tools.jar</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
			<!-- The code testing if required servers are running uses sun.jvmstat.monitor 
				package from JDK distributed tools.jar. See ActiveJavaVirtualMachineExplorer -->
			<dependencies>
				<dependency>
					<groupId>com.sun</groupId>
					<artifactId>tools</artifactId>
					<version>7</version><!-- irrelevant -->
					<scope>system</scope>
					<systemPath>${java.home}/../lib/tools.jar</systemPath>
				</dependency>
			</dependencies>
		</profile>
	</profiles>
</project>
