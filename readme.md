# GWT Playground project

Test positioning, layouts, margins, padding, dynamic centring, events, CSS styles, computed styles, box sizing, vertical text, with Google Widgets Toolkit (GWT).

Whole section is dedicated to extend GWT Panel so it can be used for easy positioning, internally handling box sizing model issues. Essential for interactive elements, like 
user resizable by dragging panels.  

Build on **z025-gwt-maven-alternative-plugin** setup. 
All examples are runnable Java files, easily launchable from IDE like Eclipse.
In Eclipse, open desired example, hit Ctrl + F11 and browser is opened with the example running. 


